#include "bbox.h";
#include "visual.h";

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <tucano/effects/phongmaterialshader.hpp>
#include <tucano/mesh.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/imageIO.hpp>
#include <tucano/utils/mtlIO.hpp>
#include <tucano/utils/objimporter.hpp>

Visual::Visual() {


}


Eigen::Vector3f Visual::getColorAtHitPoint(Tucano::Face& face, Eigen::Vector3f& origin, Eigen::Vector3f& hitLocationIn, Eigen::Vector2f& baricentricCoordinates) {

	Eigen::Vector3f resultDiffuse = Eigen::Vector3f(0, 0, 0);
	
	for (int i = 0; i < lights->size(); i++) {

		resultDiffuse += computateShading(face, origin, hitLocationIn, (*lights)[i], baricentricCoordinates) *
			computateShadow((*lights)[i], hitLocationIn);
	}

	return resultDiffuse;

}

// Method for computating the shading with an impact point with a single source of light

Eigen::Vector3f Visual::computateShading(Tucano::Face& face, Eigen::Vector3f& origin, Eigen::Vector3f& hitLocationIn, Eigen::Vector3f light, Eigen::Vector2f& baricentricCoordinates) {

	Tucano::Face hitFace = face;

	Tucano::Material::Mtl material;

	if (face.material_id < 0) {
		material = Tucano::Material::Mtl();
	}
	else {
		material = materials[face.material_id];
	}

	// 1st step - Calculate diffuse

	Eigen::Vector3f kd = material.getDiffuse();


	Eigen::Vector3f hitLocation = hitLocationIn;

	// Calculate light direction
	Eigen::Vector3f lightDirection = (light - hitLocation).normalized();

	// Calculate interpolated normal to face
	Eigen::Vector3f faceNormal = face.normal.normalized();

	
	Eigen::Vector3f normalA = mesh.getNormal(hitFace.vertex_ids[0]);
	Eigen::Vector3f normalB = mesh.getNormal(hitFace.vertex_ids[1]);
	Eigen::Vector3f normalC = mesh.getNormal(hitFace.vertex_ids[2]);
	
	faceNormal = (1 - baricentricCoordinates.x() - baricentricCoordinates.y())
		* normalA + (baricentricCoordinates.x() * normalB) + (baricentricCoordinates.y() * normalC);

	faceNormal = faceNormal.normalized();

	// Calculate 
	float angle = faceNormal.dot(lightDirection);

	if (angle < 0) {
		angle = 0;
	}

	Eigen::Vector3f diffuse = kd * angle;

	// 2nd - Specular

	// The formula used is reflection = d - (2 * (d dot n) * n) where d is the lightDirection and n is the normal (both normalized)
	Eigen::Vector3f lRef = lightDirection - 2 * (lightDirection.dot(faceNormal)) * faceNormal;
	Eigen::Vector3f eyeVector = (hitLocation - origin).normalized();

	float theta = lRef.dot(eyeVector);
	if (theta < 0) theta = 0;

	float shininess = material.getShininess();

	theta = pow(theta, shininess);

	Eigen::Vector3f specular = material.getSpecular() * theta;

	return (diffuse + specular);

}

float Visual::computateShadow(Eigen::Vector3f light, Eigen::Vector3f pointLocation) {

	Tucano::Face tempFace = Tucano::Face();
	Tucano::Face& hitTriangle = tempFace;
	Eigen::Vector3f defaultHit = Eigen::Vector3f(0, 0, 0);
	Eigen::Vector3f& hitLocation = defaultHit;

	Eigen::Vector3f outSource = pointLocation + (0.01 * (light - pointLocation));

	Eigen::Vector3f rayDirection = (light - outSource);

	//vector<Eigen::Vector3f> res = computeGreatCircle(rayDirection, light, sphereRadius, precision);
	vector<Eigen::Vector3f> res;
	if (useSoftShadows) {
		res = computeGreatCircle(rayDirection, light, lightRadius, shadowQuality);
	}
	else {
		res.push_back(light);
	}


	float hiddenParts = 0;

	Eigen::Vector2f defBarCoordinages = Eigen::Vector2f(0, 0);
	Eigen::Vector2f& baricentricCoordinates = defBarCoordinages;

	for (Eigen::Vector3f lightPoint : res) {

		float lightDistance = getVectorsDistance(lightPoint, pointLocation);

		vector<bbox> hitBoxes;
		bbox::boxIntersections(outSource, lightPoint, rootbox, hitBoxes);

		bool isHit = false;


		for (bbox current : hitBoxes) {

			if (tracer->intersectionCalculator(outSource, lightPoint, hitTriangle, hitLocation, current.triangleList, defBarCoordinages)) {

				float hitDistance = getVectorsDistance(pointLocation, hitLocation);

				if (hitDistance < lightDistance) {

					isHit = true;
				}
			}
		}

		if (isHit) hiddenParts += 1;
	}


	return 1.0 - (hiddenParts / shadowQuality);

}

vector<Eigen::Vector3f> Visual::computeGreatCircle(Eigen::Vector3f rayVector, Eigen::Vector3f circleCenter, float radius, int accuracy = 4) {

	std::vector<Eigen::Vector3f> result;

	Eigen::Vector3f vertex = rayVector.normalized();
	Eigen::Vector3f sphereInitialRadius = vertex.unitOrthogonal().normalized();

	float step = (2 * M_PI) / accuracy;

	for (int i = 0; i < accuracy; i++) {

		Eigen::AngleAxisf rotationMatrix = Eigen::AngleAxisf(step * i, vertex);
		Eigen::Vector3f pointLocation = circleCenter + ((rotationMatrix * sphereInitialRadius) * radius);
		result.push_back(pointLocation);
	}

	return result;

}


float Visual::getVectorsDistance(Eigen::Vector3f vec1, Eigen::Vector3f vec2) {

	return (vec1 - vec2).norm();
}