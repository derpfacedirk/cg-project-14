#pragma once
#ifndef __VISUAL__
#define __VISUAL__

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <tucano/effects/phongmaterialshader.hpp>
#include <tucano/mesh.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/imageIO.hpp>
#include <tucano/utils/mtlIO.hpp>
#include <tucano/utils/objimporter.hpp>
#include "IRayTrace.h"
#include "bbox.h"

class Visual
{
public:

	Visual();

	Eigen::Vector3f getColorAtHitPoint(Tucano::Face& face, Eigen::Vector3f& origin, Eigen::Vector3f& hitLocationIn, Eigen::Vector2f& baricentricCoordinates);

	IRayTrace* tracer;
	vector<Tucano::Material::Mtl> materials;
	vector<Eigen::Vector3f> *lights;
	bbox rootbox;
	Tucano::Mesh mesh;
	vector < Eigen::Vector3f>* vertex3d;
	bool useSoftShadows = true;
	float shadowQuality = 4;
	float lightRadius = 0.1;


	Eigen::Vector3f computateShading(Tucano::Face& face, Eigen::Vector3f& origin, Eigen::Vector3f& hitLocationIn, Eigen::Vector3f light, Eigen::Vector2f& baricentricCoordinates);
	float computateShadow(Eigen::Vector3f light, Eigen::Vector3f pointLocation);


private:
	float getVectorsDistance(Eigen::Vector3f vec1, Eigen::Vector3f vec2);
	vector<Eigen::Vector3f> computeGreatCircle(Eigen::Vector3f rayVector, Eigen::Vector3f circleCenter, float radius, int accuracy);

};
#endif