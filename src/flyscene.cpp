#include "flyscene.hpp"
#include <GLFW/glfw3.h>
#include <thread>
#include <math.h>
#include <chrono>
#include "visual.h"

#define MAX_LEVEL 6
#define threads 8


float epsilon = 0.0000001;


void Flyscene::initialize(int width, int height) {
	// initiliaze the Phong Shading effect for the Opengl Previewer
	phong.initialize();

	// set the camera's projection matrix
	flycamera.setPerspectiveMatrix(60.0, width / (float)height, 0.1f, 100.0f);
	flycamera.setViewport(Eigen::Vector2f((float)width, (float)height));

	// load the OBJ file and materials
	Tucano::MeshImporter::loadObjFile(mesh, materials,
		"resources/models/FinalsceneTriangulate.obj");

  // normalize the model (scale to unit cube and center at origin)
  mesh.normalizeModelMatrix();
  
	// pass all the materials to the Phong Shader
	for (int i = 0; i < materials.size(); ++i)
		phong.addMaterial(materials[i]);


	// set the color and size of the sphere to represent the light sources
	// same sphere is used for all sources
	lightrep.setColor(Eigen::Vector4f(1.0, 1.0, 0.0, 1.0));
	lightrep.setSize(0.15);

	// create a first ray-tracing light source at some rando position
	lights.push_back(Eigen::Vector3f(0, 0, 0.8));

	// scale the camera representation (frustum) for the ray debug
	camerarep.shapeMatrix()->scale(0.2);

	///* the debug ray is a cylinder, set the radius and length of the cylinder
	//ray.setSize(0.005, 10.0);*/

	// craete a first debug ray pointing at the center of the screen
	createDebugRay(Eigen::Vector2f(width / 2.0, height / 2.0));

	glEnable(GL_DEPTH_TEST);

	for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
		vertex3d.push_back(swizzle(mesh.getVertex(i)));
	}
	createBoundedBox();

	// for (int i = 0; i<10; ++i){
	//   Tucano::Face face = mesh.getFace(i);
	//   std::cout<<"face "<<i<<std::endl;
	//   for (int j =0; j<face.vertex_ids.size(); ++j){
	//     std::cout<<"vid "<<j<<" "<<face.vertex_ids[j]<<std::endl;
	//     std::cout<<"vertex "<<mesh.getVertex(face.vertex_ids[j]).transpose()<<std::endl;
	//     std::cout<<"normal "<<mesh.getNormal(face.vertex_ids[j]).transpose()<<std::endl;
	//   }
	//   std::cout<<"mat id "<<face.material_id<<std::endl<<std::endl;
	//   std::cout<<"face   normal "<<face.normal.transpose() << std::endl << std::endl;
	// }

	// Count amount of boxes:

		// Adjust the settings for the "visualizer"
	visual.lights = &lights;
	visual.materials = materials;
	visual.mesh = mesh;
	visual.vertex3d = &vertex3d;
	visual.useSoftShadows = false;
	visual.tracer = this;
	visual.shadowQuality = 10;
	visual.lightRadius = 0.15;
}

int Flyscene::count(bbox& parent){
	if (parent.children.empty()) {
		return 1;
	}
	else {
		return count(parent.children[0]) + count(parent.children[1]);
	}
		

}

void Flyscene::paintGL(void) {

	// update the camera view matrix with the last mouse interactions
	flycamera.updateViewMatrix();
	Eigen::Vector4f viewport = flycamera.getViewport();

	// clear the screen and set background color
	glClearColor(0.9, 0.9, 0.9, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// position the scene light at the last ray-tracing light source
	scene_light.resetViewMatrix();
	scene_light.viewMatrix()->translate(-lights.back());

	// render the scene using OpenGL and one light source
	phong.render(mesh, flycamera, scene_light);

	// render the ray and camera representation for ray debug
	for (int i = 0; i < rays.size(); i++) {
		rays[i].render(flycamera, scene_light);
	}
	camerarep.render(flycamera, scene_light);

	// render ray tracing light sources as yellow spheres
	for (int i = 0; i < lights.size(); ++i) {
		lightrep.resetModelMatrix();
		lightrep.modelMatrix()->translate(lights[i]);
		lightrep.render(flycamera, scene_light);
	}

	// render coordinate system at lower right corner
	flycamera.renderAtCorner();
}

void Flyscene::simulate(GLFWwindow* window) {
	// Update the camera.
	// NOTE(mickvangelderen): GLFW 3.2 has a problem on ubuntu where some key
	// events are repeated: https://github.com/glfw/glfw/issues/747. Sucks.
	float dx = (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS ? 0.1 : 0.0) -
		(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS ? 0.1 : 0.0);
	float dy = (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS ||
		glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS
		? 0.1
		: 0.0) -
		(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS ||
			glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS
			? 0.1
			: 0.0);
	float dz = (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS ? 0.1 : 0.0) -
		(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS ? 0.1 : 0.0);
	flycamera.translate(dx * 0.2, dy * 0.2, dz * 0.2);
}

void Flyscene::createDebugRay(const Eigen::Vector2f& mouse_pos) {
	rays.clear();
	Tucano::Shapes::Cylinder ray = Tucano::Shapes::Cylinder(0.1, 1.0, 16, 64);
	ray.setSize(0.005, 10.0);

	ray.resetModelMatrix();
	// from pixel position to world coordinates
	Eigen::Vector3f screen_pos = flycamera.screenToWorld(mouse_pos);

	createDebugRay(flycamera.getCenter(), screen_pos, 0);
	
  // place the camera representation (frustum) on current camera location, 
  camerarep.resetModelMatrix();
  camerarep.setModelMatrix(flycamera.getViewMatrix().inverse());

}

void Flyscene::createDebugRay(Eigen::Vector3f origin, Eigen::Vector3f dest, int level) {
	Tucano::Shapes::Cylinder ray = Tucano::Shapes::Cylinder(0.1, 1.0, 16, 64);
	ray.setSize(0.005, 10.0);
	ray.resetModelMatrix();

	Eigen::Vector3f dir = (dest - origin);

	// position and orient the cylinder representing the ray
	ray.setOriginOrientation(origin, dir);


	Tucano::Face tempFace = Tucano::Face();
	Eigen::Vector3f tempLocation = Eigen::Vector3f(0, 0, 0);

	Tucano::Face& face = tempFace;
	Eigen::Vector3f& location = tempLocation;

	// Creates list of hitboxes and populates it with the boxes that the ray goes through
	vector<bbox> hitBoxes;
	bbox::boxIntersections(origin, dest, rootBox, hitBoxes);
	hitBoxes = bbox::selectionSort(origin, hitBoxes);
	Eigen::Vector2f defBarCoordinages = Eigen::Vector2f(0, 0);
	Eigen::Vector2f& baricentricCoordinates = defBarCoordinages;
	if (hitBoxes.size() == 0) {
		cout << "no hit" << endl;
		ray.setSize(ray.getRadius(), 1000);
		rays.push_back(ray);
		return;
	}
	else {


		for (bbox current : hitBoxes) {

			if (!intersectionCalculator(origin, dest, face, location, current.triangleList, baricentricCoordinates)) {
				continue;
			}
			else {
				ray.setSize(ray.getRadius(), (location - origin).norm());
				cout << "yes hit" << endl;
				cout << visual.getColorAtHitPoint(face, origin, location, baricentricCoordinates) << endl;
				rays.push_back(ray);
				cout << rays.size() << endl;
				if (materials[face.material_id].getIlluminationModel() == 9) {
					if (!level > MAX_LEVEL) {
						Eigen::Vector3f newDest = transparentRayDest(origin, face, location, 1, level);
						createDebugRay(location, newDest, level + 1);
					}
				}
				if (materials[face.material_id].getIlluminationModel() == 3) {
					cout << "calling recursion" << endl;
					if (level < MAX_LEVEL) {
						Eigen::Vector3f newDest = reflectRayDest(face, location, dir);
						createDebugRay(location, newDest, 1);
					}
				}
				cout << "yes hit" << endl;
				cout << "color: " << visual.getColorAtHitPoint(face, origin, location, baricentricCoordinates) << endl;
				cout << "normal" << face.normal << endl;
				return;
			}
		}
	}
	ray.setSize(ray.getRadius(), 1000);
	rays.push_back(ray);
}

void Flyscene::raytraceScene(int width, int height) {
	std::cout << "ray tracing ..." << std::endl;
	std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
		);

	// if no width or height passed, use dimensions of current viewport
	Eigen::Vector2i image_size(width, height);
	if (width == 0 || height == 0) {
		image_size = flycamera.getViewportSize();
	}

	// create 2d vector to hold pixel colors and resize to match image size
	vector<vector<Eigen::Vector3f>> pixel_data;
	pixel_data.resize(image_size[1]);
	for (int i = 0; i < image_size[1]; ++i)
		pixel_data[i].resize(image_size[0]);

	thread t[threads];

	for (int i = 0; i < threads - 1; i++) {
		//thread_trace(i, image_size, results);
		t[i] = thread(&Flyscene::thread_trace, this, i, image_size, std::ref(pixel_data));
	}

	thread_trace(threads - 1, image_size, std::ref(pixel_data));

	for (int i = 0; i < threads - 1; i++) {
		t[i].join();
	}

	// write the ray tracing result to a PPM image
	Tucano::ImageImporter::writePPMImage("result.ppm", pixel_data);
	std::cout << "ray tracing done! " << std::endl;

	std::chrono::milliseconds ms2 = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
		);

	cout << ms2.count() - ms.count() << endl;
}

//void Flyscene::thread_trace(int offset, Eigen::Vector2i image_size, vector<vector<Eigen::Vector3f>> &output) {
void Flyscene::thread_trace(int offset, Eigen::Vector2i imageSize, vector<vector<Eigen::Vector3f>>& output) {


	// origin of the ray is always the camera center
	Eigen::Vector3f origin = flycamera.getCenter();
	Eigen::Vector3f screen_coords;

	//for every pixel shoot a ray from the origin through the pixel coords
	for (int j = 0; j < imageSize[1]; j++) {
		cout << "Progress: " << j << " / " << imageSize[1] << endl;
		for (int i = offset; i < imageSize[0]; i += threads) {
			// create a ray from the camera passing through the pixel (i,j)
			screen_coords = flycamera.screenToWorld(Eigen::Vector2f(i, j));
			// launch raytracing for the given ray and write result to pixel data
			output[i][j] = traceRay(origin, screen_coords);
		}
	}
}

Eigen::Vector3f Flyscene::traceRay(Eigen::Vector3f& origin,
								   Eigen::Vector3f& dest){
	return traceRay(origin, dest, 0, 1.0);
}


Eigen::Vector3f Flyscene::traceRay(Eigen::Vector3f &origin,
                                   Eigen::Vector3f &dest, int level, float opticalDensity) {

	vector<bbox> hitBoxes;
	bbox::boxIntersections(origin, dest, rootBox, hitBoxes);
	hitBoxes = bbox::selectionSort(origin, hitBoxes);
	Tucano::Face tempFace = Tucano::Face();
	Tucano::Face& hitTriangle = tempFace;
	Eigen::Vector3f defaultHit = Eigen::Vector3f(0, 0, 0);
	Eigen::Vector3f& hitLocation = defaultHit;
	
	Eigen::Vector2f defBarCoordinages = Eigen::Vector2f(0, 0);
	Eigen::Vector2f& baricentricCoordinates = defBarCoordinages;

	for (bbox p : hitBoxes) {
		
		if (intersectionCalculator(origin, dest, hitTriangle,hitLocation ,p.triangleList, baricentricCoordinates)) {
			Eigen::Vector3f shaded = visual.getColorAtHitPoint(hitTriangle, origin, hitLocation, baricentricCoordinates);;
	        if (materials[hitTriangle.material_id].getIlluminationModel() == 3.0 && level < MAX_LEVEL) {
		        Eigen::Vector3f reflected = reflectCompute(hitTriangle, hitLocation, dest-origin, opticalDensity, level);
				return shaded + reflected;
	        }
			if (materials[hitTriangle.material_id].getIlluminationModel() == 4.0 && level < MAX_LEVEL) {
				Eigen::Vector3f refracted = materials[hitTriangle.material_id].getDissolveFactor() * transparent(origin, hitTriangle, hitLocation, level, opticalDensity);
				Eigen::Vector3f reflected = reflectCompute(hitTriangle, hitLocation, dest - origin, level, opticalDensity);
				return shaded + reflected + refracted;
			}
			if (materials[hitTriangle.material_id].getIlluminationModel() == 9.0 && level < MAX_LEVEL) {
				Eigen::Vector3f refracted = transparent(origin, hitTriangle, hitLocation, level, opticalDensity);
				return 0.6*shaded + refracted;
			}
			return shaded;
		}
	}

	//return materials[hitTriangle.material_id].getAmbient();
	return Eigen::Vector3f(0.0, 0.0, 0.0);


}

bool Flyscene::intersectionCalculator(Eigen::Vector3f& origin, Eigen::Vector3f& dest, Tucano::Face& hitFace,Eigen::Vector3f& hitLocation ,vector<Tucano::Face> faces, Eigen::Vector2f& baricentricCoordinates){
	
	Eigen::Vector3f rayDirection = (dest - origin);
	bool wasHit = false;
	Tucano::Face toReturn;
	float minlength = -1;
	Eigen::Vector3f tempLocation = Eigen::Vector3f(0, 0, 0);
	Eigen::Vector3f& tempRef = tempLocation;

	Eigen::Vector2f tempBaricentric = Eigen::Vector2f(0, 0);
	Eigen::Vector2f& tempBaricentricRef = tempBaricentric;

	for (Tucano::Face current : faces) {
		if (triangleIntersect(origin, rayDirection, current, tempRef, tempBaricentricRef)) {
			float distance = (tempRef - origin).norm();
			if (distance < minlength || minlength < 0) {
				minlength = distance;
				wasHit = true;
				hitFace = current;
				hitLocation = tempRef;
				baricentricCoordinates = tempBaricentric;
			}
		}
	}
	return wasHit;
}

void Flyscene::createBoundedBox() {
	float min_x, max_x, min_y, max_y, min_z, max_z;

	min_x = min_y = min_z = std::numeric_limits<float>::max();
	max_x = max_y = max_z = std::numeric_limits<float>::lowest();
	//find min/max on every face
	for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
		if (vertex3d[i].x() < min_x) min_x = vertex3d[i].x();
		if (vertex3d[i].x() > max_x) max_x = vertex3d[i].x();
		if (vertex3d[i].y() < min_y) min_y = vertex3d[i].y();
		if (vertex3d[i].y() > max_y) max_y = vertex3d[i].y();
		if (vertex3d[i].z() < min_z) min_z = vertex3d[i].z();
		if (vertex3d[i].z() > max_z) max_z = vertex3d[i].z();
	}
	Eigen::Vector3f min_vector = Eigen::Vector3f(min_x, min_y, min_z);
	Eigen::Vector3f max_vector = Eigen::Vector3f(max_x, max_y, max_z);
	bbox boundedBox = bbox(min_vector, max_vector);

	for (int i = 0; i < mesh.getNumberOfFaces(); i++) {
		Tucano::Face face1 = mesh.getFace(i);

		for (int j = 0; j < 3; ++j) {
			Eigen::Vector3f vertex1 = vertex3d[face1.vertex_ids[j]];
			if (boundedBox.isVertexInBox(boundedBox.getminV(), boundedBox.getmaxV(), vertex1)) {
				boundedBox.triangleList.push_back(face1);
				break;
			}
		}
	}

	rootBox = boundedBox;
	boundedBox.splitBox(rootBox, mesh, vertex3d);

	visual.rootbox = rootBox;

}

bool Flyscene::triangleIntersect(Eigen::Vector3f& origin, Eigen::Vector3f& dir, Tucano::Face hitFace, Eigen::Vector3f& hitLocation, Eigen::Vector2f& baricentricCoordinates) {
	Eigen::Vector3f vertex0 = vertex3d[hitFace.vertex_ids[0]];
	Eigen::Vector3f vertex1 = vertex3d[hitFace.vertex_ids[1]];
	Eigen::Vector3f vertex2 = vertex3d[hitFace.vertex_ids[2]];

	Eigen::Vector3f edge1 = vertex1 - vertex0;
	Eigen::Vector3f edge2 = vertex2 - vertex0;

	Eigen::Vector3f h = dir.cross(edge2);
	float a = edge1.dot(h);
	if (a > -epsilon && a < epsilon) {
		return false;
	}
	float f = 1.0 / a;

	Eigen::Vector3f s = origin - vertex0;
	float u = f * s.dot(h);
	if (u < 0.0 || u > 1.0) {
		return false;
	}

	Eigen::Vector3f q = s.cross(edge1);
	float v = f * dir.dot(q);

	if (v < 0.0 || u + v > 1.0) {
		return false;
	}

	baricentricCoordinates = Eigen::Vector2f(u, v);

	float t = f * edge2.dot(q);
	if (t > epsilon && t < 1 / epsilon) {
		hitLocation = origin + dir * t;
		return true;
	}
	else {
		return false;
	}
}

Eigen::Vector3f Flyscene::reflectCompute(Tucano::Face hitFace, Eigen::Vector3f hitLocation, Eigen::Vector3f incommingRay,int level, float opticalDensity) {
	Eigen::Vector3f outgoing = reflect(incommingRay, hitFace.normal);
	Eigen::Vector3f dest = hitLocation + outgoing * 5;
	return traceRay(hitLocation, dest, level+1, opticalDensity);
}

Eigen::Vector3f Flyscene::reflect(Eigen::Vector3f base, Eigen::Vector3f reflector) {
	return base - 2 * (base.dot(reflector)) * reflector;
}

Eigen::Vector3f Flyscene::swizzle(Eigen::Vector4f toSwizzle) {
	return mesh.getShapeModelMatrix() * Eigen::Vector3f(toSwizzle.x(), toSwizzle.y(), toSwizzle.z());
}

Eigen::Vector3f Flyscene::transparent(Eigen::Vector3f origin, Tucano::Face hitFace, Eigen::Vector3f hitLocatoin, int level, float currentOpticalDensity) {

	Tucano::Material::Mtl material = materials[hitFace.material_id];
	float outgoingDensity = material.getOpticalDensity();

	Eigen::Vector3f newDest = transparentRayDest(origin, hitFace, hitLocatoin, level, currentOpticalDensity);

	Eigen::Vector3f hitLocationShift = hitLocatoin + 0.1 * (newDest - hitLocatoin);

	Eigen::Vector3f newDestShifted = newDest * 10;

	return traceRay(hitLocationShift, newDestShifted, level + 1, outgoingDensity);
}


Eigen::Vector3f& Flyscene::calculateSnellsLaw(float currentOpticalDensity, float outgoingDensity, Eigen::Vector3f normal, Eigen::Vector3f rayDirection) {


	float opRatio = currentOpticalDensity / outgoingDensity;
	Eigen::Vector3f normalCross = normal.cross(-normal.cross(rayDirection));
	float root = sqrt(1 - pow(opRatio, 2) * (normal.cross(rayDirection)).dot(normal.cross(rayDirection)));

	Eigen::Vector3f outgoing = opRatio * normalCross - root * normal;
	Eigen::Vector3f& toReturn = outgoing;
	return toReturn;
}



Eigen::Vector3f& Flyscene::transparentRayDest(Eigen::Vector3f origin, Tucano::Face hitFace, Eigen::Vector3f hitLocatoin, int level, float currentOpticalDensity) {

	Tucano::Material::Mtl material = materials[hitFace.material_id];

	Eigen::Vector3f newDirection = (-1 * (origin - hitLocatoin)).normalized();
	Eigen::Vector3f& newDirectionRef = newDirection;

	return newDirectionRef;

	float outgoingDensity = material.getOpticalDensity();
	if (currentOpticalDensity == outgoingDensity) {
		//outgoingDensity = 1;
	}

	Eigen::Vector3f normal = hitFace.normal.normalized();
	Eigen::Vector3f rayDirection = hitLocatoin - origin;

	Eigen::Vector3f unitRefractedVector = calculateSnellsLaw(currentOpticalDensity, outgoingDensity, normal, rayDirection);
	Eigen::Vector3f fixedVector = Eigen::Vector3f(-unitRefractedVector.x(), -unitRefractedVector.y(), -unitRefractedVector.z());
	Eigen::Vector3f& resultPointer = fixedVector;
	
	cout << "Debug transparency" << endl;

	cout << newDirectionRef.x() << " : " << newDirectionRef.y() << " : " << newDirectionRef.z() << endl;
	cout << resultPointer.x() << " : " << resultPointer.y() << " : " << resultPointer.z() << endl;
	

	return resultPointer;
}

Eigen::Vector3f& Flyscene::reflectRayDest(Tucano::Face hitFace, Eigen::Vector3f hitLocation, Eigen::Vector3f incommingRay) {
	Eigen::Vector3f outgoing = reflect(incommingRay, hitFace.normal);
	Eigen::Vector3f dest = hitLocation + outgoing * 5;
	Eigen::Vector3f& toReturn = dest;
	return toReturn;
}
