#pragma once
#ifndef __BBOX__
#define __BBOX__
#include <GL/glew.h>

#include <GLFW/glfw3.h>
#include <tucano/mesh.hpp>
#include <queue>
#include <algorithm>



class bbox
{
public:

	vector<bbox> children;

	std::vector<Tucano::Face> triangleList;

	bbox(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax) ;
	bbox(Eigen::Vector3f minVector, Eigen::Vector3f maxVector);
	bbox();

	void containsFace(Tucano::Face);

	Eigen::Vector3f& getminV() { return minV; }
	Eigen::Vector3f& getmaxV() { return maxV; }
	void setMinV(Eigen::Vector3f newMinV) {
		minV = newMinV;
	}
	void setMaxV(Eigen::Vector3f newMaxV) {
		maxV = newMaxV;
	}


	void splitBox(bbox& inputbox, Tucano::Mesh mesh, vector<Eigen::Vector3f> vertex3d);
	void splitBox(bbox& inputbox, Tucano::Mesh mesh, vector<Eigen::Vector3f>& vertex3d, int axis);

	
	/**
	* @brief returns which bounding boxes a ray passes through
	* @param origin Ray origin
	* @param dest Other point on the ray,
	* @param hitBoxes list of boxes to check.
	* @return Vector list with pairs of <Box, inPoints of the box>.
	 */
	static void boxIntersections(Eigen::Vector3f& origin, Eigen::Vector3f& dest, bbox& rootNode, vector<bbox>& boxesResult);
	
	/**
	* @brief Checks if ray intersects an Axis-Aligned Bounding Box.
	* @return Vector3f of inPoint if intersection, else return Vector3f(0, 0, 0)
	*/
	static bool doesBoxIntersect(Eigen::Vector3f& origin, Eigen::Vector3f dest, Eigen::Vector3f Bmin, Eigen::Vector3f Bmax);

	/**
	* @brief Sorts a vector of bboxes.
	*/
	static vector<bbox> selectionSort(Eigen::Vector3f origin, vector<bbox> boxesResult);


	static bool isVertexInBox(Eigen::Vector3f& Vmin, Eigen::Vector3f& Vmax, Eigen::Vector3f& V) {
		if (V.x() >= Vmin.x() && V.x() <= Vmax.x()) {
			if (V.y() >= Vmin.y() && V.y() <= Vmax.y()) {
				if (V.z() >= Vmin.z() && V.z() <= Vmax.z()) {
					return true;
				}
			}
		}
		return false;
	}

private:
	Eigen::Vector3f minV;
	Eigen::Vector3f maxV;

};
#endif
