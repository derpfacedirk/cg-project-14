#pragma once
#ifndef __IRAYTRACE__
#define __IRAYTRACE__

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <tucano/effects/phongmaterialshader.hpp>
#include <tucano/mesh.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/imageIO.hpp>
#include <tucano/utils/mtlIO.hpp>
#include <tucano/utils/objimporter.hpp>

class IRayTrace {
public:
	virtual bool intersectionCalculator(Eigen::Vector3f& origin, Eigen::Vector3f& dest, Tucano::Face& hitFace, Eigen::Vector3f& hitLocation, vector<Tucano::Face> faces, Eigen::Vector2f& barimetric) = 0;
};

#endif