#include "bbox.h"
#include "flyscene.hpp"

#define maximumT 6000

bbox::bbox(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax) {
	minV = Eigen::Vector3f(xmin, ymin, zmin);
	maxV = Eigen::Vector3f(xmax, ymax, zmax);
}

bbox::bbox(Eigen::Vector3f minVector, Eigen::Vector3f maxVector) {
	minV = minVector;
	maxV = maxVector;
}

bbox::bbox() {}

void bbox::containsFace(Tucano::Face triangle) {
	//triangle.vertex_ids[0]
}

// Goes through the bbox tree and returns a list with the boxes the ray intersects with.
void bbox::boxIntersections(Eigen::Vector3f& origin, Eigen::Vector3f& dest, bbox& rootNode, vector<bbox>& boxesResult) {
	//cout << "boxIntersections has been called." << endl;
	Eigen::Vector3f rayDirection = (dest - origin);
	

	if (rootNode.children.empty()) {
		if (bbox::doesBoxIntersect(origin, dest, rootNode.getminV(), rootNode.getmaxV())) {
			boxesResult.push_back(rootNode);
			// cout << "box has been added to boxesResult" << endl;
		}
	}
	else {
		// Loop over every box in the rootNode.children list.
		// cout << "we gotta loop over the children" << endl;
		if (bbox::doesBoxIntersect(origin, dest, rootNode.getminV(), rootNode.getmaxV())) {
			if (bbox::doesBoxIntersect(origin, dest, rootNode.children[0].getminV(), rootNode.children[0].getmaxV())) {
				bbox::boxIntersections(origin, dest, rootNode.children[0], boxesResult);
			}
			if (bbox::doesBoxIntersect(origin, dest, rootNode.children[1].getminV(), rootNode.children[1].getmaxV())) {
				bbox::boxIntersections(origin, dest, rootNode.children[1], boxesResult);
			}
		}
	}
}


vector<bbox> bbox::selectionSort(Eigen::Vector3f origin, vector<bbox> boxesResult) {
	int listSize = boxesResult.size();
	for (int i = 0; i < listSize - 1; i++) {
		bbox currentMin = boxesResult[i];
		int currentMinIndex = i;
		for (int j = i + 1; j < listSize; j++) {
			Eigen::Vector3f currentMinV = currentMin.getminV() - origin;
			Eigen::Vector3f boxesMinV = boxesResult[j].getminV() - origin;
			if (currentMinV.norm() > boxesMinV.norm()) { 
				currentMin = boxesResult[j];
				currentMinIndex = j;
			}
		}
		// swap the list[i] with list[currentMinIndex]
		if (currentMinIndex != i) {
			boxesResult[currentMinIndex] = boxesResult[i];
			boxesResult[i] = currentMin;
		}
	}

	return boxesResult;
}


bool bbox::doesBoxIntersect(Eigen::Vector3f& origin, Eigen::Vector3f dest, Eigen::Vector3f Bmin, Eigen::Vector3f Bmax) {
	Eigen::Vector3f rayDirection = (dest - origin);

	float xMin = Bmin.x();
	float yMin = Bmin.y();
	float zMin = Bmin.z();
	float xMax = Bmax.x();
	float yMax = Bmax.y();
	float zMax = Bmax.z();

	if (origin.x() >= xMin && origin.x() <= xMax && origin.y() <= yMax && origin.y() >= yMin && origin.z() <= zMax && origin.z() >= zMin) {
		return true;
	}

	float TxMin = (xMin - origin.x()) / rayDirection.x();
	float TxMax = (xMax - origin.x()) / rayDirection.x();
	float TyMin = (yMin - origin.y()) / rayDirection.y();
	float TyMax = (yMax - origin.y()) / rayDirection.y();
	float TzMin = (zMin - origin.z()) / rayDirection.z();
	float TzMax = (zMax - origin.z()) / rayDirection.z();

	float TxIn = min(TxMin, TxMax);
	float TxOut = max(TxMin, TxMax);

	float TyIn = min(TyMin, TyMax);
	float TyOut = max(TyMin, TyMax);

	float TzIn = min(TzMin, TzMax);
	float TzOut = max(TzMin, TzMax);

	float Tin = max(TxIn, max(TyIn, TzIn));
	float Tout = min(TxOut, min(TyOut, TzOut));

	return !(Tin > Tout || Tin < 0);

}

void bbox::splitBox(bbox& inputbox, Tucano::Mesh mesh, vector<Eigen::Vector3f> vertex3d) {
	splitBox(inputbox, mesh, vertex3d, 1);
}

void bbox::splitBox(bbox& inputbox, Tucano::Mesh mesh, vector<Eigen::Vector3f>& vertex3d, int axis) {
	cout << "Size of trianglelist: " << inputbox.triangleList.size() << endl;

	
	if (inputbox.triangleList.size() <= maximumT) {
		//boxVector.push_back(inputbox);
		return;
	}

	// get Vmin and Vmax
	Eigen::Vector3f minV = inputbox.getminV();
	Eigen::Vector3f maxV = inputbox.getmaxV();

	// Get largest axis
	float x1 = maxV.x() - minV.x();
	float y1 = maxV.y() - minV.y();
	float z1 = maxV.z() - minV.z();
	int largestAxis = 1;
	if (y1 > x1) {
		largestAxis = 2;
	}
	if (z1 > x1 && z1 > y1) {
		largestAxis = 3;
	}

	// Get average triangle position over largest axis
	float totalT = 0;
	for (int i = 0; i < inputbox.triangleList.size(); i++) {
		Tucano::Face face1 = inputbox.triangleList[i];
		float avgTriangle = 0;
		for (int j = 0; j < 3; j++) {
			if (largestAxis == 1) avgTriangle += vertex3d[face1.vertex_ids[j]].x();
			if (largestAxis == 2) avgTriangle += vertex3d[face1.vertex_ids[j]].y();
			if (largestAxis == 3) avgTriangle += vertex3d[face1.vertex_ids[j]].z();
		}
		totalT += avgTriangle/3;
	}
	float average1 = (float) totalT / (float) inputbox.triangleList.size();

	// Create the splitted boxes.
	bbox first; 	
	bbox second; 
	
	// Constructor of the boxes. Set their position 
	if (largestAxis == 1) {
		first = bbox(minV.x(), average1, minV.y(), maxV.y(), minV.z(), maxV.z());
		second = bbox(average1, maxV.x(), minV.y(), maxV.y(), minV.z(), maxV.z());
	}
	if (largestAxis == 2) {
		first = bbox(minV.x(), maxV.x(), minV.y(), average1, minV.z(), maxV.z());
		second = bbox(minV.x(), maxV.x(), average1, maxV.y(), minV.z(), maxV.z());
	}
	if (largestAxis == 3) {
		first = bbox(minV.x(), maxV.x(), minV.y(), maxV.y(), minV.z(), average1);
		second = bbox(minV.x(), maxV.x(), minV.y(), maxV.y(), average1, maxV.z());
	}


	// Create the triangleList for each inputBox.
	for (int i = 0; i < inputbox.triangleList.size(); i++) {
		Tucano::Face face1 = inputbox.triangleList[i];
		bool inbox1 = false;
		bool inbox2 = false;
		for (int j = 0; j < 3; j++) {
			Eigen::Vector3f currentVertex = vertex3d[face1.vertex_ids[j]];
			//Eigen::Vector3f currentVertex = Eigen::Vector3f(V.x(), V.y(), V.z());
			if (bbox::isVertexInBox(first.getminV(), first.getmaxV(), currentVertex) && !inbox1) {
				inbox1 = true;
				first.triangleList.push_back(face1);
				continue;
			}
			if (bbox::isVertexInBox(second.getminV(), second.getmaxV(), currentVertex) && !inbox2) {
				inbox2 = true;
				second.triangleList.push_back(face1);
				continue; 
			}
		}
	}

	// Assign the children to parent
	inputbox.children.push_back(first);
	inputbox.children.push_back(second);

	bbox firstChild = inputbox.children[0];
	bbox secondChild = inputbox.children[1];

	if (firstChild.triangleList.size() == 0 || secondChild.triangleList.size() == 0) {
		return;
	}

	largestAxis++;
	if (largestAxis > 3) {
		largestAxis = 1;
	}

	// Don't split if amount of triangles smaller than maximum
	if (first.triangleList.size() >= maximumT) {
		splitBox(firstChild, mesh, vertex3d);
	}
	if (second.triangleList.size() >= maximumT) {
		splitBox(secondChild, mesh, vertex3d);
	}
}



